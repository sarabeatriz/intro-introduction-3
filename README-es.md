
# Introducción al laboratorio y a los programados que utilizaremos


![main1.png](images/main1.png)

Esta es la primera de una serie de experiencias de laboratorio que se desarrollaron como parte del proyecto patrocinado por la NSF "Development of engaging and readily transferable laboratory experiences for the introductory programming course" (DUE-1245744). El objetivo principal de estas experiencias de laboratorio es que los estudiantes de cursos introductorios de programación practiquen los conceptos y habilidades aprendidas en clase y obtener productos atractivos que los motivarán a explorar aún más las infinitas oportunidades de diversión y aplicabilidad de la Ciencia de Cómputos.

Cada una de las experiencias de laboratorio utiliza bibliotecas y código reutilizable desarrollado por profesores y estudiantes del programa de Ciencia de Cómputos de la Universidad de Puerto Rico, en el campus de Río Piedras (http://ccom.uprrp.edu) (http: //ccom.uprrp .edu)). Cuando se complete cada experiencia de laboratorio, el alumno no sólo reforzará los conceptos aprendidos en clase, sino que también podrá ver los productos que él o ella puede lograr con un poco más de conocimiento y experiencia en computación.

En esta primera experiencia de laboratorio aprenderá el flujo de trabajo para las próximas actividades, cómo acceder a los archivos que se utilizarán y cómo entregar sus trabajos. También aprenderá a gestionar los elementos básicos de Qt, la plataforma que nos permitirá desarrollar y ejecutar proyectos en C++.


## Objetivos:

1. Conocer sobre las experiencias de laboratorio preparadas y las expectativas generales de las sesiones de laboratorio.

2. Practicar el uso de *Git* y *Bitbucket*, bajar, y guardar los archivos necesarios para cada experiencia de laboratorio.

3. Practicar cómo compilar, corregir errores y ejecutar un programa usando Qt.

---

## Servicios y programados que utilizaremos:

### Libro electrónico

Las instrucciones para las experiencias de laboratorio están contenidas en el libro electrónico que puedes acceder en [http://eip.ccom.uprrp.edu/_book-en/](http://eip.ccom.uprrp.edu/_book-en/).

Publicaremos el nombre del laboratorio cada semana en nuestra página de Moodle del curso.

Esta es una versión preliminar del libro y por lo tanto puede contener errores. Te agradeceremos que nos indiques si encuentras algún error y si tienes alguna sugerencia que pueda ayudar a que las instrucciones sean más claras. Por favor notifiquenos a: eip.uprrp@upr.edu.


### Bitbucket y Git

*Bitbucket* es un repositorio o depósito de archivos digitales al que se puede acceder en línea y que permite trabajar proyectos en grupo de
manera ordenada y simple. Los archivos del Laboratorio de Introducción a la Programación estarán almacenados en este lugar y se podrán bajar a
las computadoras personales utilizando *Git*.

*Git* es un sistema de control de versiones de código abierto que es usado principalmente para el desarrollo de software. El equipo de EIP utilizó *Git* durante el desarrollo de software de las experiencias de laboratorio. Usted como estudiante posiblemente no necesitará usar las capacidades de control de versiones de *Git* para las asignaciones de código en las experiencias de laboratorio.  Sin embargo, usted utilizará *Git* para bajar el código fuente de los archivos de las experiencias de laboratorio al comienzo de la sesión de laboratorio, por ejemplo, usando `git clone url`. Puede obtener *Git* en [http://git-scm.com/](http://git-scm.com/).


### Terminal y Linux

Los ejercicios de programación desarrollados por EIP se pueden completar en cualquiera de los tres sistemas operativos principales: Linux, OSX y Windows. Independientemente de su sistema operativo preferido, hemos encontrado que el dominio de la línea de comandos siempre ayuda a los estudiantes a completar los ejercicios de programación más cómodamente. Los siguientes son algunos comandos útiles para el terminal OSX y Linux.

---

|    **Comando**     |                      **Acción**                     |
| ------------------ | --------------------------------------------------- |
| ls                 | muestra lista de los archivos en el directorio      |
| mv nombre1 nombre2 | mueve contenido de nombre1 a archivo nombre2 (cambia nombre del archivo)       |
| cp nombre1 nombre2 | copia contenido de nombre1 a archivo nombre2        |
| rm nombre          | borra archivo                                       |
| mkdir nombre       | crea directorio nombre dentro del directorio actual |
| cd ..              | cambia al directorio anterior                       |
| cd ~               | cambia al directorio hogar                          |
| cd nombre          | cambia al directorio nombre (dentro del actual)     |
| flecha hacia arriba| repite comando anterior                             |

           
**Figura 1.** Comandos básicos de Unix.

---


### Qt


Qt es un marco de aplicaciones multiplataforma ampliamente utilizado por muchas organizaciones para desarrollar aplicaciones multiplataforma. C++ es el lenguaje de programación principal de Qt. La marco QT funciona en, y puede crear versiones de, aplicaciones para diferentes plataformas (tanto plataformas de escritorio, plataformas móviles, entre otras). El proyecto Qt proporciona un entorno de desarrollo integrado (IDE), denominado Qt Creator. Le recomendamos que instale Qt y Qt Creator en su computadora personal y explore las opciones que ofrecen estas aplicaciones. También le animamos a inspirarse explorando la diversidad de aplicaciones desarrolladas por la comunidad Qt en [https://showroom.qt.io/](https://showroom.qt.io/).


### Máquina virtual

El proyecto EIP distribuye todos sus productos (libros electrónicos, código fuente, etc.) como una máquina virtual disponible en [http://eip.ccom.uprrp.edu/vms/](http://eip.ccom.uprrp.edu/vms/). El archivo comprimido contiene un archivo VMDK que se puede reproducir en VMware Workstation o VirtualBox.

Le recomendamos que utilice la máquina virtual, ya que viene preinstalada con Qt, el libro en línea y directorios de código fuente para las diversas experiencias de laboratorio. Además, el código fuente ha sido probado con la versión Qt que está preinstalada. Si instala otra versión de Qt, usted puediese tener problemas con algunos de los programas.


## Cómo obtener Qt

Si decide instalar la aplicación Qt de forma nativa en su computadora, puede descargarla desde la página del proyecto Qt en [http://qt-project.org/](http://qt-project.org/). Nuestro proyecto desarrolló algunos videos que le instruyen sobre cómo instalar Qt en los distintos sistemas operativos en: [http://youtu.be/6eY5VSPYZCw](http://youtu.be/6eY5VSPYZCw) (Linux), [http://youtu.be/CImDCSxi7Wc](http://youtu.be/CImDCSxi7Wc) (Windows), [http://youtu.be/_zq-pSw3Ox4](http://youtu.be/_zq-pSw3Ox4) (Mac).

---

---

## Usando Qt

En esta experiencia de laboratorio aprenderemos a utilizar algunas de las funcionalidades básicas de Qt Creator. Utilizaremos Qt Creator principalmente para editar y depurar nuestros programas. Sin embargo, Qt Creator también incluye Qt Designer, la herramienta Qt para diseño de GUI que se utilizó para diseñar las interfaces gráficas de las experiencias de laboratorio. Aprender a usar esta herramienta no es parte de este curso, pero puedes aprender a usarlo por tu cuenta. En [https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk](https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk) puede encontrar una presentación, preparada por el estudiante Jonathan Vélez, que muestra los conceptos básicos de usar Qt Creator y Qt Designer para diseñar interfaces gráficas.


### Proyectos en C++

Cada proyecto en C++ se compone de varios tipos de archivos. En proyectos de Qt
comunmente tendrás archivos del tipo *fuente (source), encabezados (header) y
formulario (form)*.

-   **Archivos "source":** Estos archivos tienen una extensión `.cpp` (C plus plus) y contienen el código fuente de C++ para su programa. Entre estos archivos encontrará `main.cpp`; Este es el archivo que normalmente contiene la función principal que el preprocesador buscará para iniciar su programa. Otro archivo fuente que encontrará en los proyectos creados con Qt es el archivo `mainwindow.cpp`; Este archivo es creado por Qt y contiene la implementación de las funciones que controlan el código asociado con la ventana GUI principal diseñada con la opción de diseño (por ejemplo, las funciones que aparecen debajo de "Private slots").

-   **Archivos "headers":** Estos archivos tienen extensión `.h` y contienen declaraciones de las funciones que son utilizadas en el programa. 

-   **Archivos "forms":** Estos archivos tienen extensión `.ui` (user interface) y contienen una descripción de los controles y widgets incluidos en la ventana GUI de la aplicación.



---

---


## Sesión de laboratorio

En esta experiencia de laboratorio practicarás el uso de algunos de los programados que utilizarás durante el semestre. Utilizarás los programas que están instalados en la máquina virtual.

### Ejercicio 0: Crear directorio para los archivos de los laboratorios

Utiliza el terminal y el comando `mkdir` para crear un directorio `Documents/eip` para los archivos de los laboratorios. 


### Ejercicio 1: Comenzar proyecto nuevo

#### Instrucciones

1. Para comenzar un proyecto en C++, marca el botón de `New Project` o ve al menú principal de Qt Creator y en `File` selecciona `New File or Project`. Saldrá una ventana similar a la ventana en la Figura 2. Selecciona `Non-Qt Project`, `Plain C++ Project` y marca `Choose`.

    ---

    ![figure2.png](images/figure2.png)

    **Figura 2.** Comenzar proyecto en C++ sin aplicaciones gráficas.

    ---

2. Escribe el nombre del proyecto, selecciona el directorio en donde quieres guardarlo, marca `Next` en esa y la siguiente ventana, y luego `Finish` en la próxima.

    Este proceso creará un nuevo proyecto en Qt con el esqueleto de un programa básico en C++ que solo despliega "Hello World!". Antes de continuar, selecciona `Projects` en el menú vertical de la izquierda. Deberá aparecer la pantalla de `Build Settings`. En esa pantalla, asegúrate que la cajita de `Shadow build` NO esté seleccionada, como ocurre en la Figura 3.

    ---

    ![figure3.png](images/figure3.png)

    **Figura 3.** La opción `Shadow build` no está seleccionada.

    ---

3. Regresa a la pantalla donde puedes editar el programa seleccionando `Edit` en el menú de la izquierda y haciendo "doble click" en `Sources` y luego en `main.cpp`. Presiona la flecha verde en el menú de la izquierda para ejecutar el programa. Los resultados del programa se desplegarán en una pantalla de terminal. Si hubiese errores, estos aparecerán en la pantalla de `Issues` en Qt Creator.

4. Cambia el contenido de `main.cpp` para que sea:

    ```cpp
    #include <iostream>
    using namespace std;

    int main()
    {
        cout << endl << "Me gusta el laboratorio de programacion." << endl;
        return 0;
    }
    ```
    ---

5. Marca el botón verde del menú de la izquierda para compilar y ejecutar el programa. Saldrá una ventana que te ofrece la opción de guardar los cambios. Marca `Save all`. Al ejecutar, si no cometiste ningún error, el programa debe desplegar "Me gusta el laboratorio de programacion." en la pantalla de terminal.


### Ejercicio 2: Bajar proyectos de Bitbucket

Los archivos para cada experiencia de laboratorio están guardados en Bitbucket. En cada sesión de laboratorio bajarás de Bitbucket la carpeta que contiene los archivos de esa experiencia de laboratorio y los guardarás en el directorio `Documents/eip` que creaste en el Ejercicio 0. Para bajar la carpeta correspondiente a esta experiencia de laboratorio, abre un terminal, utiliza los comandos de Linux para cambiar de directorio al directorio `Documents/eip` y ejecuta el comando `git clone https://bitbucket.org/eip-uprrp/intro-introduction.git`. Ve al directorio `Documents/eip` y verifica que contiene la carpeta `intro-introduction`.


### Ejercicio 3: Abrir proyecto ya creado, compilar, y ejecutar

En este ejercicio practicarás cómo compilar, corregir errores, y ejecutar un programa usando Qt. 

#### Instrucciones

1. Primero borrarás los archivos que fueron creados por Qt durante el proceso de construcción y cerrarás los archivos del proyecto anterior. Para hacer esto, en el menú principal de Qt Creator ve a `Build`  y selecciona `Clean all`; luego ve a `File` y selecciona `Close all projects and editors`. 

2. Carga a `QtCreator` el proyecto `Practica`  haciendo doble "click" en  el archivo `Practica.pro` en el directorio `Documents/eip/introduccion` de tu computadora. En la ventana que aparece marca `Configure Project`.

    Cada vez que cargues o comiences algún proyecto asegúrate de que el `Shadow build` no esté seleccionado: en el menú de la izquierda, selecciona `Projects` y luego, en `Build Settings`, verifica que la cajita de `Shadow build` no esté seleccionada, como vimos en la Figura 3.

3. Como viste anteriormente, Qt te permite construir y ejecutar el programa marcando la flecha verde que aparece en la columna de la izquierda. Presiona la flecha y nota que obtienes una ventana de "Issues" que ocurrieron al construir. La lista que aparece te muestra información que te permitirá encontrar y corregir los errores. 

4. Selecciona el archivo `main.cpp` en el directorio de `Sources` para que puedas encontrar y corregir los errores. 

    Corrige todos los errores y presiona nuevamente la flecha verde para construir y ejecutar el programa. Una vez corrijas todos los errores, el programa debe abrir la pantalla `Application Output` y desplegar `Salida: 1`.

5. Como mencionamos antes, durante el proceso de compilación y ejecución, Qt crea varios archivos que debemos borrar luego de terminar con el programa. Para hacer esto, en la opción `Build` del menú de Qt Creator, selecciona `Clean All`.


### Ejercicio 4: Entrega de trabajos

Durante cada experiencia de laboratorio el estudiante deberá entregar algunos resultados de su trabajo. Estas entregas se harán en la sección de "Entregas" que aparece en Moodle. Hoy cada estudiante practicará una entrega individualmente. 


#### Instrucciones

1. Abre el enlace de "Entregas" en Moodle y entrega el archivo `main.cpp`. Recuerda utilizar buenas prácticas de programación incluyendo el nombre de los programadores como comentario al inicio de tu programa.


